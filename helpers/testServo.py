# https://tutorials-raspberrypi.com/raspberry-pi-servo-motor-control/


import RPi.GPIO as GPIO
import time

servoPIN = 18
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

servo = GPIO.PWM(servoPIN, 50)              # set up the pin for PWM with 50Hz
servo.start(0)                            	# start the servo with the signal offInitialization


def sweep_arm():
	servo.ChangeDutyCycle(5)			# move the servo arm a bit to the left
	time.sleep(1)
	servo.ChangeDutyCycle(7.5)			# move the servo arm forward
	time.sleep(1)
	servo.ChangeDutyCycle(5)			# return the servo arm a bit to the left
	time.sleep(1)
	
try:
	while True:
		sweep_arm()
		# servo.ChangeDutyCycle(5)			# move the servo arm a bit to the left
		# time.sleep(1)
		# servo.ChangeDutyCycle(7.5)			# move the servo arm forward
		# time.sleep(1)

except KeyboardInterrupt:
	servo.stop()
	GPIO.cleanup()

