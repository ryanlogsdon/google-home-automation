# Adafruit with the feed for this example is: io.adafruit.com/ryanlogsdon/dashboards/fishlights

import time
from Adafruit_IO import MQTTClient

adafruit_user = 'ryanlogsdon'
adafruit_key = 'aio_KYVE55OThSyEiJ4QZaI2bWIYTgRq'
client = MQTTClient(adafruit_user, adafruit_key)
IO_FEED = 'onoff'
IO_FEED_USERNAME = 'ryanlogsdon'

# callback functions

def connected(client):
    client.subscribe(IO_FEED, IO_FEED_USERNAME)

def disconnected(client):
    print('Disconnected from Adafruit IO!')
    sys.exit(1)

def message(client, feed_id, payload):              # this will be called when a subscribed feed has a new value
    print(f'Feed {feed_id} received new value: {payload}')
    

# set up the callbacks

client.on_connect = connected
client.on_disconnect = disconnected
client.on_message = message



     
if __name__ == '__main__':
	
    client.connect()                                    # connect to Adafruit server
    client.loop_background()
    
    print('Publishing a new message every 10 seconds (press Ctrl-C to quit)...')
    while True:
        # value = random.randint(0, 100)
        # print('Publishing {0} to {1}.'.format(value, IO_FEED))
        # client.publish(IO_FEED, value, IO_FEED_USERNAME)
        time.sleep(10)



	
	