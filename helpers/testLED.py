import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
LED = 17                            # GPIO17 (pin #6)
GPIO.setup(LED, GPIO.OUT)

light_status = False

if __name__ == '__main__':
    try:
        while True:
            light_status = not light_status
            GPIO.output(LED, light_status)
            time.sleep(0.5)
    except KeyboardInterrupt:
        print('goodbye!')
