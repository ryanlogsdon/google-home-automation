# best tutorials
# https://adafruit-io-python-client.readthedocs.io/en/latest/quickstart.html
# instructables.com/Control-Your-Projects-With-Google-Assistant-and-Ad/




from argparse import ArgumentParser
from Adafruit_IO import Client

adafruit_user = 'ryanlogsdon'
adafruit_key = 'aio_KYVE55OThSyEiJ4QZaI2bWIYTgRq'
aio = Client(adafruit_user, adafruit_key)

read_value = False
toggle_value = False

parser = ArgumentParser()
parser.add_argument('-r', '--read', dest='read_value', action='store_true', help='read the value')
parser.add_argument('-t', '--toggle', dest='toggle_value', action='store_true', help='toggle the current value')
args = parser.parse_args()

if args.read_value:
	read_value = True
if args.toggle_value:
	toggle_value = True

if __name__ == '__main__':
	if read_value:
		data = aio.receive('onoff')					# retrieve the most recent value from feed 'onoff'
		print(f'Received value: {data.value}')		# all values are retrieved as strings, so cast if you need numbers
	
	if toggle_value:
		data = aio.receive('onoff')
		current_value = data.value
		print(f'Started with value: {current_value}')

		if current_value == 'On':
			aio.send('onoff', 'Off')
		else:
			aio.send('onoff', 'On')

		data = aio.receive('onoff')
		print(f'New value: {data.value}')
	
	