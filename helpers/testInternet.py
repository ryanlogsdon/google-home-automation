import socket
import time

def is_connected():
    try:
        socket.create_connection(("1.1.1.1", 53))
        return True
    except OSError:
        pass
    return False


if __name__ == '__main__':
    while not is_connected():
        print(f'not online...')
        time.sleep(2)
    print(f"we're online!")
