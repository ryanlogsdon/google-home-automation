import sys
import time
import socket                                                       # to test for an Internet connection when we run on reboot!
from Adafruit_IO import MQTTClient                                  # pip3 install adafruit-io
import RPi.GPIO as GPIO

ADAFRUIT_IO_USERNAME = "MY_USERNAME"
ADAFRUIT_IO_KEY = "MY_SECRET_KEY"
IO_FEED = 'MY_DATA_FEED'

GPIO.setmode(GPIO.BCM)
LED = 17                                                            # GPIO17 (pin #6)
GPIO.setup(LED, GPIO.OUT)


# MQTT callback functions

def connected(client):
    client.subscribe(IO_FEED, ADAFRUIT_IO_USERNAME)

def disconnected(client):
    print('Disconnected from Adafruit IO! Goodbye!')
    sys.exit(1)

def message(client, feed_id, payload):                              # this will be called when a subscribed feed has a new value
    print(f'Feed {feed_id} received new value: {payload}')
    if payload == 'ON':
        print('toggle turned ON')
        toggle_LED()
    if payload == 'OFF':
        print('toggle turned OFF')
        toggle_LED()

# helper functions

def toggle_LED():
    global light_status
    print('toggling the LED')
    light_status = not light_status                                 # toggle the LED from off to on, or on to off
    GPIO.output(LED, light_status)

def is_connected():
    try:
        socket.create_connection(("1.1.1.1", 53))
        return True
    except OSError:
        pass
    return False


if __name__ == '__main__':
    client = MQTTClient(ADAFRUIT_IO_USERNAME, ADAFRUIT_IO_KEY)
    client.on_connect = connected                                   # set up the callbacks
    client.on_disconnect = disconnected
    client.on_message = message

    light_status = False                                            # False = the LED will be off, True = the LED will be on

    while not is_connected():                                       # make sure we have Internet (we may launch on reboot!)
        print(f'trying to get online...')
        time.sleep(2.0)
    print(f'we are online!')

    client.connect()                                                # connect to Adafruit server
    client.loop_background()
    print('Publishing a new message every 10 seconds (press Ctrl-C to quit)...')

    try:
        while True:
            # value = random.randint(0, 100)
            # print('Publishing {0} to {1}.'.format(value, IO_FEED))
            # client.publish(IO_FEED, value, IO_FEED_USERNAME)
            time.sleep(10)
    except KeyboardInterrupt:
        print('disconnecting...')
        client.disconnect()

	
	