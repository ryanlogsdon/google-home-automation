good tutorial:      https://www.instructables.com/Control-Your-Projects-With-Google-Assistant-and-Ad/
Raspberry Pi pins:  https://roboticsbackend.com/raspberry-pi-3-pins/

set up accounts with:
    https://io.adafruit.com/
    https://ifttt.com/

Adafruti setup:

    Adafruit IO > Feeds > view all > "+ New Feed" > Name =  "ledlight" 

    Adafruit IO > Dashboards > view all > "+ New Dashboard" > Name =  "led light dashboard" 

    led light dasboard > Settings (gear) > Create new block > choose "toggle" > select our "ledlight" feed > Next Step
        Block Settings > (make any desired changes) > Create Block

    Adafruit IO > My Key > copy the "scripting" lines of code

Python installs:

    pip3 install adafruit-io

If-This-Then-That setup:

    IFTTT > Create > If This "Add" > (write in) "google" and select Google Assistant > simple phrase 
                   > Then That "Add" > Adafruit > Send data to Adafruit IO > (choose the feed)


    Add a 2nd IFTTT applet for the "OFF" command


